import { ThemeTypes } from "../../shared/constants";

export interface ISetThemeActions {
  readonly type: "SET_THEME";
  payload?: ThemeTypes;
}

export type ThemeActions = ISetThemeActions;
