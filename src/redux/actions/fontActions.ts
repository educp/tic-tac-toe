import { FontTypes } from "../../shared/constants";

export interface ISetFontActions {
  readonly type: "SET_FONT";
  payload?: FontTypes;
}

export type FontActions = ISetFontActions;
