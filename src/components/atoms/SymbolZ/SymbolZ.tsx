import React from "react";

import "./SymbolZ.scss";

import * as Types from "./SymbolZTypes";

const SymbolZ: React.FC<Types.SymbolZProps> = ({
  value,
  action,
  lenght,
}: Types.SymbolZProps) => (
  <React.Fragment>
    <input
      type="text"
      value={value}
      className="input"
      onChange={action}
      maxLength={lenght}
    />
  </React.Fragment>
);

export default SymbolZ;
