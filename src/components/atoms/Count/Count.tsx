import React from "react";

import "./Count.scss";

import * as Types from "./CountTypes";

const Count: React.FC<Types.CountProps> = ({ value }: Types.CountProps) => (
  <React.Fragment>
    <span id="count" className="count">
      {value}
    </span>
  </React.Fragment>
);

export default Count;
