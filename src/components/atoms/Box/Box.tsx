import React from "react";

import * as Types from "./BoxTypes";

const Box: React.FC<Types.BoxProps> = ({ children }: Types.BoxProps) => (
  <div
    className="children"
    style={{ border: "20px solid #000", padding: "20px" }}
  >
    {children}
  </div>
);

export default Box;
