import React from "react";

export interface BoxProps {
  children: React.ReactNode;
}
