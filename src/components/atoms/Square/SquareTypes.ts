export type SquareValue = "O" | "X";

export interface SquareProps {
  key: number;
  value?: string;
  handleOnClick: React.MouseEventHandler<HTMLButtonElement>;
}
