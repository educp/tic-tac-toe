import React from "react";

import { useGlobalSizes } from "../../../shared/context";

const ContextSelectedTheme: React.FC = () => {
  const { size } = useGlobalSizes();
  return (
    <div
      style={{
        border: "solid 6px white",
        width: size,
        height: size,
        transition: "all linear 1s",
      }}
    >
      CONTEXT
    </div>
  );
};

export default ContextSelectedTheme;
