import React from "react";

import "./Restart.scss";

import * as Types from "./RestartTypes";

const Restart: React.FC<Types.RestartProps> = ({
  value,
  handleOnClick,
}: Types.RestartProps) => (
  <React.Fragment>
    <button className="restart" onClick={handleOnClick}>
      {value}
    </button>
  </React.Fragment>
);

export default Restart;
