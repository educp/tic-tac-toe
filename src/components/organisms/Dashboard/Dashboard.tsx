import React, { useState } from "react";

import Game from "../../molecules/Game/Game";
import Box from "../../atoms/Box/Box";

import { GlobalSizesContext, Sizes } from "../../../shared/context";

import SwitchSelector from "../../molecules/SwitchSelector/SwitchSelector";

const Dashboard: React.FC = () => {
  const [size, setSize] = useState<Sizes>(Sizes.SSize);

  function renderGame() {
    return (
      <React.Fragment>
        <Box>{<Game />}</Box>
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      <GlobalSizesContext.Provider value={{ size, setSize }}>
        <SwitchSelector />
        {renderGame()}
      </GlobalSizesContext.Provider>
    </React.Fragment>
  );
};

export default Dashboard;
