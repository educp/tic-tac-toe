import { createContext, useContext } from "react";

export enum Sizes {
  LSize = "300px",
  SSize = "100px",
}

type GlobalSizes = {
  size?: Sizes;
  setSize?: (size: Sizes) => void;
};

export const GlobalSizesContext = createContext<GlobalSizes>({});

export const useGlobalSizes = (): GlobalSizes => useContext(GlobalSizesContext);
