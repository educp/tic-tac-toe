export enum ThemeTypes {
  dark = "#000",
  bright = "#fff",
}

export enum FontTypes {
  small = "10px",
  huge = "40px",
}
